#include "field.h"

Field::Field()
{
    value = std::nullopt;
}

Field::Field(const std::string& s)
{
    if (s.size() == 0) {
        value = std::nullopt;
    } else if (s[0] == '"') {
        value = s.substr(1, s.size() - 2);
    } else if (s.find('.') != std::string::npos) {
        try {
            value = std::stod(s);
        } catch (...) {
            value = std::nullopt;
        }
    } else {
        if (s == "true") {
            value = true;
        } else if (s == "false") {
            value = false;
        } else {
            if (isdigit(s[0])) {
                try {
                    value = std::stol(s);
                } catch (...) {
                    value = std::nullopt;
                }
            } else{
                value = s;
            }
        }
    }
}

Field::FieldType Field::getType() const {
    switch (value.index()) {
        case 0:
            return INTEGER;
        case 1:
            return FLOAT;
        case 2:
            return BOOLEAN;
        case 3:
            return STRING;
    }
    return UNKNOWN;
}

std::string Field::convertToString() const
{
    switch (value.index()) {
        case 0:
            return std::to_string(getInteger());
        case 1:
            return std::to_string(getFloat());
        case 2:
            return std::string(getBool() ? "true" : "false");
        case 3:
            return getString();
    }
    return "";
}

void Field::setValue(int64_t x)
{
    value = x;
}

void Field::setValue(double x)
{
    value = x;
}

void Field::setValue(bool x)
{
    value = x;
}

void Field::setValue(std::string x)
{
    value = x;
}

void Field::setNull()
{
    value = std::nullopt;
}

bool Field::operator==(const Field& y) const
{
    if (getType() != y.getType()) return false;
    switch (getType()) {
        case INTEGER:
            return getInteger() == y.getInteger();
        case FLOAT:
            // Comparing floats for equalness is not safe.
            return false;
        case BOOLEAN:
            return getBool() == y.getBool();
        case STRING:
            return getString() == y.getString();
        case UNKNOWN:
            // Two unknows are allways equal.
            return true;
    }
    return false;    
}
