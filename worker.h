#pragma once

#include <thread>
#include "log4cxx/logger.h"
#include "runnable.h"
#include "threadsafe_queue.h"

class Worker
{
public:
    Worker();
    virtual ~Worker();

    void give_order(Runnable* runnable);

private:
    ThreadsafeQueue<Runnable*> queue;
    bool working;
    std::thread my_thread;
    log4cxx::LoggerPtr logger;
    static int thread_nr;

    void job_handler();
};