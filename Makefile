# Makefile for project

# Directory for binaries
BINDIR = bin/

# Directory for object files
OBJDIR = bin/

# Directory for source files
SRCDIR = ./

# These are the source files to compile
SRCS = 	chinook.cpp \
		worker.cpp \
		dispatcher.cpp \
		configuration.cpp \
		httprunner.cpp \
		utils.cpp \
		filegetter.cpp \
		database.cpp \
		table.cpp \
		record.cpp \
		field.cpp \
		main.cpp \
		request.cpp \
		test.cpp \
		where.cpp \
		authorizer.cpp

# Name of executable to build
EXECUTABLE = chinook

# Libraries to link with
LIBS = -llog4cxx -lpthread -lssl -lcrypto
#LIBS = -llog4cxx -lpthread -lssl -lcrypto -lstdc++fs (On Debian/Ubuntu)

# Every source file compiles to one object file
OBJS = $(SRCS:.cpp=.o)

# This is a list of object files with its directory
OWD = $(addprefix $(OBJDIR),$(OBJS))

# Rule to compile source files
$(OBJDIR)%.o: $(SRCDIR)%.cpp
	g++ -std=c++17 -pthread -pedantic -Wall -c -ggdb -o $@ $<
	
all: 	clean $(BINDIR)$(EXECUTABLE)

clean:
	rm -f $(BINDIR)*
	clear

test: $(BINDIR)$(EXECUTABLE)
	$(BINDIR)$(EXECUTABLE)

$(BINDIR)$(EXECUTABLE): $(OWD)
	g++ -std=c++17 $(OWD) $(LIBS) -ggdb -o $(BINDIR)$(EXECUTABLE)
