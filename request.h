#pragma once

#include <string>
#include <vector>
#include <map>
#include <optional>

// This class contains the HTTP METHOD and URL
class Request
{
public:

    enum Method {
        UNKNOWN,
        GET,
        POST,
        PUT,
        DELETE
    };

    Request();
    void init(std::string method, std::string url);

    Method getMethod() const;
    std::string getMethodString() const;
    std::string getPath() const;
    std::string getPathPart(size_t n) const;
    std::map<std::string, std::string> getQuery() const;
    std::optional<int> getQueryValueAsInt(std::string key);
    std::optional<std::string> getQueryValueAsString(std::string key);

private:
    Method method;
    std::vector<std::string> path;
    std::map<std::string, std::string> query;
};