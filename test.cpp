#include "test.h"

Test::Test() :
    Test(Comparator::EQUAL, Field(""))
{
}

Test::Test(Comparator comparator, Field field) :
    comparator(comparator),
    field(field)
{
}

Test::Test(std::string& s) :
    field(s)
{
    if ((s.size() > 4) && (s[2] == '(') && (s[s.size() - 1] == ')')) {
        if (s.substr(0,2) == "ne") {
            comparator = NOT_EQUAL;
        } else {
            comparator = EQUAL;
        }
        field = Field(s.substr(3, s.size() - 4));
    } else {
        comparator = EQUAL;
    }
}

bool Test::test(Field x)
{
    switch (comparator) {
        case EQUAL:
            return x == field;
        case NOT_EQUAL:
            return !(x == field);
    }
    return false;
}
