#pragma once

#include <string>

// Version of this software
static const std::string VERSION = "0.4.0";

// Listening on this port unless otherwise configured.
static const int DEFAULT_HTTP_PORT = 8080;
static const int DEFAULT_HTTPS_PORT = 4433;

// Keeping this number of connections in backlog unless otherwise configured.
static const int DEFAULT_BACKLOG = 20;

// Number of worker threads handling incoming requests.
static const int DEFAULT_HTTP_WORKERS = 4;
static const int DEFAULT_HTTPS_WORKERS = 10;

// TCP connection will be closed after this many seconds.
static const int DEFAULT_CONNECTION_TIMEOUT = 5;

// This is where the html files can be found.
static const std::string DEFAULT_HTTP_DOCUMENT_ROOT = "/var/www/chinook/http";
static const std::string DEFAULT_HTTPS_DOCUMENT_ROOT = "/var/www/chinook/https";

// TLS (HTTPS) settings.
static const std::string DEFAULT_SERVER = "localhost";
static const std::string CERTIFICATE_FILENAME_POSTFIX = ".cert.pem";
static const std::string PRIVATE_KEY_FILENAME_POSTFIX = ".key.pem";

// Database files can be found in this directory.
static const std::string DEFAULT_DATABASE_DIRECTORY = "database";

// Database query pagination, default pagesize.
static const int DEFAULT_PAGESIZE = 1000;

// Table name of the users database for authorization.
static const std::string USERS_TABLE = "users";
