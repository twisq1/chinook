#pragma once

#include <string>
#include <vector>
#include "log4cxx/logger.h"
#include "base.h"

class Authorizer
{
public:
    Authorizer();
    void init();    // Do allow all reads.
    void init(const std::vector<std::string>& metafile);
    bool isAuthorized(const std::string& authorization_header, bool write);
    std::string getRealm() const;

private:
    log4cxx::LoggerPtr logger;
    std::string realm;
    std::vector<std::string> allowedReadGroups;
    std::vector<std::string> allowedWriteGroups;
    bool noInfo;
};