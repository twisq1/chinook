#include "where.h"

Where::Where()
{
}

void Where::addTest(const std::string& key, std::string& test)
{
    tests[key] = Test(test);
}

std::optional<Test> Where::getTest(std::string key) const
{
    auto it = tests.find(key);
    if (it == tests.end()) {
        return std::nullopt;
    } else {
        return std::make_optional<Test>(it->second);
    }
}
