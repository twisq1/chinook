#pragma once

#include <string>
#include <map>
#include <optional>
#include "test.h"

class Where
{
public:
    Where();

    void addTest(const std::string& key, std::string& test);
    std::optional<Test> getTest(std::string) const;
    std::map<std::string, Test>& getTests() { return tests; }

private:
    std::map<std::string, Test> tests;
};