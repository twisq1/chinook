#pragma once

#include <memory>
#include <string>
#include <map>
#include <thread>
#include "log4cxx/logger.h"
#include <openssl/ssl.h>
#include <openssl/err.h>

class Chinook
{
public:
    Chinook(int port, bool tls, const std::string document_root, int workers);
    virtual ~Chinook();

    void start();
    void stop();

    // This method is called when the client sends a server name in its request (SNI).
    int servername_callback(SSL *ssl);

private:
    int port;
    bool tls;
    std::string document_root;
    int workers;
    log4cxx::LoggerPtr logger;
    int sockFD;
    bool on;
    std::map<std::string, SSL_CTX*> ssl_contexts;
    std::thread my_thread;

    void run();
    void init_openssl();
    void cleanup_openssl();

    // Create SSL contect for this servername.
    SSL_CTX* get_context(const std::string servername);
    std::string getOpenSSLError();
};