#pragma once

#include <string>
#include "field.h"

class Test
{
public:
    enum Comparator {
        EQUAL,
        NOT_EQUAL
    };

    Test();
    Test(Comparator comparator, Field field);
    Test(std::string& s);
    bool test(Field x);

private:
    Comparator comparator;
    Field field;
};