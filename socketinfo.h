#pragma once

#include <sys/socket.h>

struct SocketInfo
{
    int socket_descriptor;
    sa_family_t family_type;
    void* address;
};