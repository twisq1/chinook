#include <signal.h>
#include <thread>
#include <chrono>
#include <log4cxx/logger.h>
#include "configuration.h"
#include "chinook.h"
#include "database.h"
#include "config.h"

using namespace std::chrono_literals;

Chinook* http = nullptr;
Chinook* https = nullptr;

bool running = true;

// Handler to catch Ctrl-C and terminate.
void sighandler(int s)
{
    if (http) http->stop();
    if (https) https->stop();
    running = false;
}

void saveAll()
{
    auto db = Database::getInstance();
    for (auto table : db->getTables()) {
        table->save();
    }
}

int main(int argc, char *argv[])
{
    log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("main");
    LOG4CXX_INFO(logger, "CHINOOK version " << VERSION);

    // Get configuration parameters.
    Configuration::getInstance()->initialize(argc, argv);

    // Load database into memory.
    Database::getInstance()->load();

    // Add handler for Ctrl-C and sigterm.
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = sighandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    sigaction(SIGTERM, &sigIntHandler, NULL);

    // Initialize and start HTTP server.
    std::string http_document_root = Configuration::getInstance()->getString("http_document_root").value_or(DEFAULT_HTTP_DOCUMENT_ROOT);
    int http_port = Configuration::getInstance()->getInteger("http_port").value_or(DEFAULT_HTTP_PORT);
    int http_workers = Configuration::getInstance()->getInteger("http_workers").value_or(DEFAULT_HTTP_WORKERS);
    if (http_port) {
        http = new Chinook(http_port, false, http_document_root, http_workers);
        http->start();
    }
    
    // Initialize and start HTTPS server.
    std::string https_document_root = Configuration::getInstance()->getString("https_document_root").value_or(DEFAULT_HTTPS_DOCUMENT_ROOT);
    int https_port = Configuration::getInstance()->getInteger("https_port").value_or(DEFAULT_HTTPS_PORT);
    int https_workers = Configuration::getInstance()->getInteger("https_workers").value_or(DEFAULT_HTTPS_WORKERS);
    if (https_port) {
        https = new Chinook(https_port, true, https_document_root, https_workers);
        https->start();
    }
    
    int counter = 0;
    while(running) {
        std::this_thread::sleep_for(20ms);
        counter++;

        if (counter > 3000) {
            //Save all database tables every minute.
            saveAll();
            counter = 0;
        }
    }

    LOG4CXX_DEBUG(logger, "Shutting down.");
    saveAll();
    if (http) delete http;
    if (https) delete https;
}
