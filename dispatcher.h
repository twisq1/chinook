#pragma once

#include <vector>
#include "log4cxx/logger.h"
#include "runnable.h"
#include "worker.h"

class Dispatcher
{
public:

    Dispatcher(const int number_of_workers);
    virtual ~Dispatcher();

    void give_order(Runnable* runnable);

private:
    log4cxx::LoggerPtr logger;
    std::vector<Worker*> workers;
    std::vector<Worker*>::iterator worker_on_duty;
};