#include <sstream>
#include "utils.h"
#include "request.h"

Request::Request()
{
}

void Request::init(std::string methodstring, std::string url)
{
    if (methodstring == "GET") {
        method = GET;
    } else if (methodstring == "PUT") {
        method = PUT;
    } else if (methodstring == "POST") {
        method = POST;
    } else if (methodstring == "DELETE") {
        method = DELETE;
    } else {
        method = UNKNOWN;
    }

    std::string pathstring;
    size_t q = url.find('?');
    if (q != std::string::npos) {
        pathstring = url.substr(0, q);

        std::string allquerystring = url.substr(q + 1);
        query.clear();
        for (auto querystring : split(allquerystring, '&')) {
            auto keyvalue = split(querystring, '=');
            if (keyvalue.size() == 2) {
                query[keyvalue[0]] = keyvalue[1];
            }
        }
    } else {
        pathstring = url;
    }

    if (pathstring.size() > 1) {
        path = split(pathstring.substr(1), '/');
    } else {
        path.clear();
    }
}

Request::Method Request::getMethod() const
{
    return method;
}

std::string Request::getMethodString() const
{
    switch(method) {
        case GET:
            return "GET";
        case PUT:
            return "PUT";
        case POST:
            return "POST";
        case DELETE:
            return "DELETE";
        default:
            return "UNKNOWN";
    }
}

std::string Request::getPath() const
{
    std::stringstream ss;
    bool empty = true;
    for (std::string s : path) {
        ss << "/" << s;
        empty = false;
    }
    if (empty) {
        ss << "/";
    }
    return ss.str();
}

std::string Request::getPathPart(size_t n) const
{
    if (path.size() > n) {
        return path[n];
    } else {
        return "";
    }
}
std::map<std::string, std::string> Request::getQuery() const
{
    return query;
}

std::optional<int> Request::getQueryValueAsInt(std::string key)
{
    auto s = getQueryValueAsString(key);
    if (s.has_value()) {
        int value = 0;
        try {
            value = std::stoi(s.value());
            return std::make_optional(value);
        } catch(...) {
            // Prevent exception from stopping program. Instead return nullopt.
        }
    }
    return std::nullopt;
}

std::optional<std::string> Request::getQueryValueAsString(std::string key)
{
	if (query.find(key) == query.end())	
	{
		return std::nullopt;
	}
	else return std::make_optional(query[key]);
}