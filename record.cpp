#include <sstream>
#include "table.h"
#include "utils.h"
#include "record.h"

Record::Record(Table& table) :
    table(table)
{
    logger = log4cxx::Logger::getLogger("record");
    id = nullptr;
}

Record::~Record()
{
    //LOG4CXX_DEBUG(logger, "Record destroyed with id " << id);
}

void Record::addField(std::shared_ptr<Field> field)
{
    if (id == nullptr) {
        if (field->getType() != Field::INTEGER) {
            // First field must be the id and it must be an integer. Force if not.
            field->setValue(static_cast<int64_t>(0));
        }
        id = field;
    }
    values.push_back(field);
}

uint64_t Record::getId() const
{
    if (id != nullptr) {
        return id->getInteger();
    } else {
        LOG4CXX_WARN(logger, "Record has invalid id.");
        return 0;
    }
}

void Record::setId(uint64_t x)
{
    if (id != nullptr) {
        id->setValue(static_cast<int64_t>(x));
    } else {
        LOG4CXX_WARN(logger, "Record has invalid id.");
    }
}

std::shared_ptr<Field> Record::getValue(int n) const
{
    return values[n];
}

std::shared_ptr<Field> Record::getValue(const std::string& key) const
{
    auto it = values.begin();
    for (auto column : table.getColumns()) {
        if (column.columnName == key) {
            return *it;
        }
        it++;
    }
    return nullptr;
}


std::string Record::str() const
{
    std::stringstream ss;

    bool first = true;
    for (std::shared_ptr<Field> field : values) {
        if (!first) ss << ",";
        ss << ((field != nullptr) ? field->convertToString() : "null");
        first = false;
    }
    return ss.str();
}

std::string Record::csv() const
{
    std::stringstream ss;

    bool first = true;
    for (std::shared_ptr<Field> field : values) {
        if (!first) ss << ",";
        if (field != nullptr) {
            switch(field->getType()) {
            case Field::INTEGER:
                ss << field->getInteger();
                break;
            case Field::FLOAT:
                ss << field->getFloat();
                break;
            case Field::BOOLEAN:
                ss << (field->getBool() ? "true" : "false");
                break;
            case Field::STRING:
                ss << '"' << field->getString() << '"';
                break;
            case Field::UNKNOWN:
                // Empty field
                break;
            }
        }
        first = false;
    }
    return ss.str();
}

nlohmann::json Record::json() const
{
    nlohmann::json json;
    int i = 0;
    for (Table::ColumnDefinition column : table.getColumns()) {
        std::shared_ptr<Field> value = getValue(i++);
        switch(value->getType()) {
            case Field::INTEGER:
                json[column.columnName] = value->getInteger();
                break;
            case Field::FLOAT:
                json[column.columnName] = value->getFloat();
                break;
            case Field::BOOLEAN:
                json[column.columnName] = value->getBool();;
                break;
            case Field::STRING:
                json[column.columnName] = value->getString();
                break;
            case Field::UNKNOWN:
                json[column.columnName] = nullptr;
        }
    }
    return json;
}

bool Record::init(nlohmann::json& json)
{
    for (auto column : table.getColumns()) {
        auto key = column.columnName;
        if (json.contains(key)) {
            auto value = json[key];
            // TODO: Initialize field with json object directly.
            std::shared_ptr<Field> field = std::shared_ptr<Field>(new Field(value.dump()));
            addField(field);
        } else {
            std::shared_ptr<Field> field = std::shared_ptr<Field>(new Field());
            addField(field);
            LOG4CXX_DEBUG(logger, key << " not found.");
        }
    }
    return true;
}

void Record::update(nlohmann::json& json)
{
    auto it = values.begin();
    for (auto column : table.getColumns()) {
        // Do not allow updating id field.
        if (*it != id) {
            auto key = column.columnName;
            if (json.contains(key)) {
                auto value = json[key];
                LOG4CXX_DEBUG(logger, "Updating " << key << " with " << value << ".");
                switch (column.columnType) {
                    case Field::BOOLEAN:
                        (*it)->setValue(static_cast<bool>(value));
                        break;
                    case Field::INTEGER:
                        (*it)->setValue(static_cast<int64_t>(value));
                        break;
                    case Field::FLOAT:
                        (*it)->setValue(static_cast<double>(value));
                        break;
                    case Field::STRING:
                        (*it)->setValue(static_cast<std::string>(value));
                        break;
                    default:
                        (*it)->setNull();
                        break;
                }
            }
        }
        it++;
    }
    table.setDirty();
}

// Check if the where statement is true for this record.
bool Record::check(Where& where)
{
    auto tests = where.getTests();
    auto field = values.begin();
    for (auto column : table.getColumns()) {
        if (field == values.end()) {
            debug("Inconsistent record.");
            return false;
        }
        auto test = tests.find(column.columnName);
        if (test != tests.end()) {
            if (!test->second.test(**field)) {
                // Test has failed.
                return false;
            }
        }
        field++;
    }
    // No test has been failed.
    return true;
}

bool Record::hasId() const
{
    return (id != nullptr);
}
