* CHINOOK *
Yet another webserver

Install dependencies on Debian with:

> apt-get update
> apt-get install -y g++
> apt-get install -y liblog4cxx
> apt-get install -y liblog4cxx-dev
> apt-get install -y libssl-dev

Create certificate and private key with:
> openssl req -x509 -nodes -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365

Enable lower port numbers like 80 for non root users:
> sudo setcap CAP_NET_BIND_SERVICE=+eip bin/chinook

Add this file to the linker:
> -lstdc++fs

When running on MacOS, you can install dependencies with:

> brew install log4cxx

Remove this file from the linker:
> -lstdc++fs

Build and run:
> make
> bin/chinook http_port=1234 http_document_root=/var/www
