#pragma once

#include <algorithm> 
#include <string>
#include <vector>
#include <climits>

std::vector<std::string> split(const std::string &line, char delimeter);
bool strings_equal_ignore_case(std::string& str1, std::string &str2);
bool strings_equal_ignore_case(std::string& str1, const char* str2);

// trim from start (in place)
inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

std::string string_replace(const std::string& str, const std::string& match, const std::string& replacement, unsigned int max_replacements = UINT_MAX);

void debug(std::string s);