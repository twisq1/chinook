#pragma once

class Runnable
{
public:
    virtual ~Runnable()
    {
    }

    virtual void operator() () = 0;
};