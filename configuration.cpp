#include "configuration.h"
#include <sstream>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

shared_ptr<Configuration> Configuration::instance = shared_ptr<Configuration>(new Configuration());

shared_ptr<Configuration> Configuration::getInstance()
{
	return instance;
}

Configuration::Configuration()
{
	logger = log4cxx::Logger::getLogger("configuration");
	LOG4CXX_DEBUG(logger, "Configuration object constructed.");
}

Configuration::~Configuration()
{
	LOG4CXX_DEBUG(logger, "Configuration object destructed.");
}

void Configuration::initialize(int argc, char* argv[])
{
	for(int i=1 ; i<argc ; i++)
	{
		string oneline = argv[i];
		string key;
		string value;
		if (stringtokeyvalue(oneline, key, value))
		{
			if (key == "config")
			{
				ifstream file;
				file.open(value.c_str(), ifstream::in);
				if (file.is_open())
				{
					while (getline(file, oneline))
					{
						if (oneline[0] != '#')
						{
							if (stringtokeyvalue(oneline, key, value))
							{
								insertkeyvalue(key, value);
							}
						}
					}
				}
				else
				{
					LOG4CXX_ERROR(logger, "Cannot open config file: "<< value);
				}
			}
			else 
			{
				insertkeyvalue(key, value);
			}
		}
	}
}

bool Configuration::stringtokeyvalue(string in, string& key, string &value)
{
	size_t i = in.find('=', 0);
	if (i != string::npos)
	{
		key = in.substr(0, i);
		value = in.substr(i+1, -1);
	} else {
		key = in;
		value = "true";
	}
	return true;
}

void Configuration::insertkeyvalue(string key, string value)
{
	// Test if value is an integer
	bool no_integer = false;
	for(string::const_iterator it = value.begin(); it != value.end(); ++it)
	{
		if((*it < '0') || (*it > '9')) no_integer = true;
	}
	if (no_integer)
	{
		strings.insert(std::pair<string, string>(key, value));
	}
	else
	{
		istringstream ss(value);
		int32_t number;
		ss >> number;
		integers.insert(std::pair<string, uint32_t>(key, number));
	}
	LOG4CXX_DEBUG(logger, "Config value added: " << key << "=" << value << (no_integer ? " as string" : " as integer"));
}

std::optional<int> Configuration::getInteger(string key)
{
	if (integers.find(key) == integers.end()) 
	{
		return std::nullopt;
	}
	else return std::make_optional(integers[key]);
}

std::optional<std::string> Configuration::getString(string key)
{
	if (strings.find(key) == strings.end())	
	{
		return std::nullopt;
	}
	else return std::make_optional(strings[key]);
}

std::optional<bool> Configuration::getBool(string key)
{
	if (strings.find(key) == strings.end())
	{
		return std::nullopt;
	}
	else return (getString(key) == "true");
}
