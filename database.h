#pragma once

#include <memory>
#include "log4cxx/logger.h"
#include "table.h"
#include "config.h"

class Database {

public:
    static std::shared_ptr<Database> getInstance();
    virtual ~Database();

    void load();
    std::shared_ptr<Table> getTable(std::string tablename);
    std::vector<std::shared_ptr<Table>> getTables();

private:
    Database();
    static std::shared_ptr<Database> instance;

    log4cxx::LoggerPtr logger;
    std::map<std::string, std::shared_ptr<Table>> tables;
};