#pragma once

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <mutex>
#include "log4cxx/logger.h"
#include "record.h"
#include "where.h"
#include "authorizer.h"
#include "field.h"

class Table
{
public:
    struct ColumnDefinition {
        Field::FieldType columnType;
        std::string columnName;
    };

    Table(std::string name);
    virtual ~Table();

    // Load table from file into memory.
    bool load();
    bool save();
    std::shared_ptr<Record> getRecord(uint64_t id);
    std::vector<ColumnDefinition> getColumns() const { return columns; }
    std::vector<std::shared_ptr<Record>> getRecords(Where& where);
    Authorizer& getAuthorizer() { return authorizer; }
    std::shared_ptr<Record> addRecord(std::shared_ptr<Record> record);
    bool deleteRecord(uint64_t id);
    void setDirty() { dirty = true; };

private:
    log4cxx::LoggerPtr logger;
    std::string tableName;
    std::vector<ColumnDefinition> columns;
    std::map<uint64_t, std::shared_ptr<Record>> records;
    Authorizer authorizer;
    std::mutex mut;
    uint64_t maxid;
    bool dirty;

    // Helper functions
    inline void removeQuotes(std::string& s) {
        if ((s.size() > 2) && (s[0] == '"')) s = s.substr(1, s.size() - 2);
    };

    // Split by comma, but respect quoted values and unescape them.
    std::vector<std::string> smart_split(const std::string line) const;
};