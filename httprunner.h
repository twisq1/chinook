#pragma once

#include <sys/socket.h>
#include <openssl/ssl.h>
#include "log4cxx/logger.h"
#include "httpresponsecode.h"
#include "filegetter.h"
#include "runnable.h"
#include "request.h"

//#define LOG_FULL_REQUEST
const char RESPONSE_200[] = "<html><head><title>Everything okay</title></head><body><h1>200 - OK</h1><p>Everyting went fine.</p></body></html>";
const char RESPONSE_301[] = "<html><head><title>Permamnent redirect</title></head><body><h1>301 - Permanent redirect</h1><p>This file has been moved to <a href=\"$location\">another location</a>.</p></body></html>";
const char RESPONSE_302[] = "<html><head><title>Temporary redirect</title></head><body><h1>302 - Temporary redirect</h1><p>This file has been moved to <a href=\"$location\">another location</a>.</p></body></html>";
const char RESPONSE_400[] = "<html><head><title>Bad Request</title></head><body><h1>400 - Bad Request</h1></body></html>";
const char RESPONSE_401[] = "<html><head><title>Not authorized</title></head><body><h1>401 - Not authorized</h1></body></html>";
const char RESPONSE_404[] = "<html><head><title>File not found</title></head><body><h1>404 - File not found</h1></body></html>";

class HttpRunner : public Runnable
{
public:
    static const int MAX_PAYLOAD_SIZE = 1024 * 1024;

    HttpRunner(const std::string document_root, int socket, sa_family_t family_type, void* address, SSL* ssl = nullptr);
    virtual ~HttpRunner();
    virtual void operator() ();

private:
    std::string document_root;
    int socket_descriptor;
    sa_family_t family_type;
    void* address;
    SSL* ssl;
    log4cxx::LoggerPtr logger;

    char payload[MAX_PAYLOAD_SIZE];
    char send_buffer[MAX_PAYLOAD_SIZE];
    size_t payload_size;
    char* body;

    Request request;
    std::string protocol;
    std::string host;
    std::string user_agent;
    std::string accept;
    std::string accept_language;
    std::string connection;
    std::string authorization;

    bool parse_headers();
    bool parse_header_line(const std::string line, bool firstline);
    bool check_redirection(std::vector<std::string>& lines);

    void handle_file_request();
    void handle_db_get_request();
    void handle_db_post_request();
    void handle_db_put_request();
    void handle_db_delete_request();

    void send_response(HttpResponseCode code, std::string content_type = "", const char* data = nullptr, const size_t size = 0);
    void send_response(std::string header, const char* data, const size_t size);
    void send_test_response();
    void send_unauthorized(const std::string& realm);
    void send_redirect_response(bool permanent, std::string location);
};