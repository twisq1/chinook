#include <sstream>
#include <cctype>
#include <locale>
#include <iostream>
#include "log4cxx/logger.h"
#include "utils.h"

std::vector<std::string> split(const std::string &line, char delimeter)
{
    std::stringstream stream(line);
    std::string item;
    std::vector<std::string> elements;

    while (getline(stream, item, delimeter)) {
        elements.push_back(move(item));
    }
    return elements;
}

bool strings_equal_ignore_case(std::string& str1, std::string& str2)
{
    return ((str1.size() == str2.size()) && std::equal(str1.begin(), str1.end(), str2.begin(), [](char & c1, char & c2){
                            return (c1 == c2 || std::toupper(c1) == std::toupper(c2));
                                }));
}

bool strings_equal_ignore_case(std::string& str1, const char* str2)
{
    std::string s2(str2);
    return strings_equal_ignore_case(str1, s2);
}

std::string string_replace(const std::string& str, const std::string& match, const std::string& replacement, unsigned int max_replacements)
{
    size_t pos = 0;
    std::string newstr = str;
    unsigned int replacements = 0;
    while ((pos = newstr.find(match, pos)) != std::string::npos
            && replacements < max_replacements)
    {
         newstr.replace(pos, match.length(), replacement);
         pos += replacement.length();
         replacements++;
    }
    return newstr;
}

void debug(std::string s)
{
    log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("debug");
    LOG4CXX_DEBUG(logger, s);
}