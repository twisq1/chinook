#pragma once

#include <memory>
#include <map>
#include <optional>
#include <string>
#include "log4cxx/logger.h"

class Configuration
{
private:
	Configuration();
	static std::shared_ptr<Configuration> instance;

	log4cxx::LoggerPtr logger;
	std::map<std::string, int32_t> integers;
	std::map<std::string, std::string> strings;

	bool stringtokeyvalue(std::string in, std::string& key, std::string &value);
	void insertkeyvalue(std::string key, std::string value);

public:
	static std::shared_ptr<Configuration> getInstance();
	void initialize(int argc, char* argv[]);
	~Configuration();
	
	std::optional<int> getInteger(std::string key);
	std::optional<std::string> getString(std::string key);
	std::optional<bool> getBool(std::string key);

};
