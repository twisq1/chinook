#pragma once

#include <string>

class HttpResponseCode
{
public:
    enum Code {
        OK,
        BAD_REQUEST,
        NOT_FOUND
    };

    HttpResponseCode(Code code) :
        code(code)
    {}

    std::string toString() const
    {
        switch(code) {
            case OK:
                return "200 OK";
            case BAD_REQUEST:
                return "400 Bad Request";
            case NOT_FOUND:
                return "404 Not Found";
        }
        return "";
    }

private:
    Code code;
};