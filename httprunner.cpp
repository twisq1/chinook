#include <arpa/inet.h>
#include <unistd.h>
#include <string_view>
#include <cstring>
#include "utils.h"
#include "filegetter.h"
#include "configuration.h"
#include "config.h"
#include "database.h"
#include "where.h"
#include "authorizer.h"
#include "httprunner.h"

HttpRunner::HttpRunner(const std::string document_root, int socket, sa_family_t family_type, void* address, SSL* ssl) :
    document_root(document_root),
    socket_descriptor(socket),
    family_type(family_type),
    address(address),
    ssl(ssl)
{
    logger = log4cxx::Logger::getLogger("httprunner");
}

HttpRunner::~HttpRunner()
{
}

void HttpRunner::operator() ()
{
    // We got a new connection. Get client IP address.
    std::string ip;
    if (family_type == AF_INET) {

        char ip4[INET_ADDRSTRLEN];  // space to hold the IPv4 string
        inet_ntop(AF_INET, address, ip4, INET_ADDRSTRLEN);
        ip = ip4;

    } else if (family_type == AF_INET6) {

        char ip6[INET6_ADDRSTRLEN];  // space to hold the IPv6 string
        inet_ntop(AF_INET6, address, ip6, INET6_ADDRSTRLEN);
        ip = ip6;

    } else {

        LOG4CXX_ERROR(logger, "Connection from non IP client.");
        close(socket_descriptor);
        return;
    }

    //LOG4CXX_DEBUG(logger, "New TCP connection from " << ip);
        
    ssize_t received = 0;
    do {

        if (ssl) {
            received = SSL_read(ssl, payload, MAX_PAYLOAD_SIZE);
        } else {
            received = recv(socket_descriptor, payload, MAX_PAYLOAD_SIZE, 0);
        }

        if (received > 0) {
            payload_size = received;
            payload[payload_size] = '\0';   // Add terminator so we can use standard string functions.

            #ifdef LOG_FULL_REQUEST
                std::string s(payload, payload_size);
                LOG4CXX_DEBUG(logger, "Full request:\n" << s);
            #endif

            if (parse_headers()) {
                LOG4CXX_INFO(logger, ip << " - " << host << " - " << request.getMethodString() << " - " << request.getPath());

                // Handle request
                if (request.getMethod() == Request::GET) {
                    if (request.getPathPart(0) == "info") {
                        send_test_response();
                    } else if (request.getPathPart(0) == "api") {
                        handle_db_get_request();
                    } else {
                        handle_file_request();
                    }
                } else if ((request.getMethod() == Request::POST) && (request.getPathPart(0) == "api")) {
                    handle_db_post_request();
                } else if ((request.getMethod() == Request::PUT) && (request.getPathPart(0) == "api")) {
                    handle_db_put_request();
                } else if ((request.getMethod() == Request::DELETE) && (request.getPathPart(0) == "api")) {
                    handle_db_delete_request();
                } else {
                    LOG4CXX_WARN(logger, "Unsupported method");
                }

            } else {
                LOG4CXX_ERROR(logger, "Illegal request from " << ip);
                send_response(HttpResponseCode(HttpResponseCode::BAD_REQUEST), "text/html", RESPONSE_400, sizeof(RESPONSE_400));
            }
        } else {
            //LOG4CXX_DEBUG(logger, "Zero bytes received or timeout.");
        }

    } while (received > 0);
    LOG4CXX_DEBUG(logger, "Closing TCP socket.");

    if (ssl) {
        SSL_shutdown(ssl);
        SSL_free(ssl);
    }
    close(socket_descriptor);
}

void HttpRunner::handle_file_request()
{
    std::string url = request.getPath();
    if (url == "/") url = "/index.html";
    FileGetter filegetter(document_root);

    // First try to load meta file.
    bool ok = true;
    Authorizer authorizer;
    authorizer.init();  // This enables all reads.
    size_t start_of_extension = url.find_last_of('.');
    if (start_of_extension != url.npos) {
        std::string metafilename = url.substr(0, start_of_extension) + ".meta";
        if (filegetter.load(host, metafilename)) {
            //LOG4CXX_DEBUG(logger, "Metafile loaded: " << metafilename);
            std::string meta(filegetter.getData(), filegetter.getSize());
            std::vector<std::string> lines = split(meta, '\n');
            ok = check_redirection(lines);
            authorizer.init(lines);
        }
    }

    if (ok) {
        if (authorizer.isAuthorized(authorization, false)) {
            if (filegetter.load(host, url)) {
                //LOG4CXX_DEBUG(logger, "File loaded.");
                send_response(HttpResponseCode(HttpResponseCode::OK), filegetter.getMime(), filegetter.getData(), filegetter.getSize());
            } else {
                LOG4CXX_INFO(logger, "File not found.");
                send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
            }
        } else {
            // Not authorized.
            send_unauthorized(authorizer.getRealm());
        }
    }
}

void HttpRunner::handle_db_get_request()
{
    std::string tablename = request.getPathPart(1);
    std::string ids = request.getPathPart(2);
    auto database = Database::getInstance();
    auto table = database->getTable(tablename);
    if (table == nullptr) {
        LOG4CXX_DEBUG(logger, "Table does not exist.");
        send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
        return;
    }

    // Check authorization
    if (!table->getAuthorizer().isAuthorized(authorization, (request.getMethod() != Request::GET))) {
        send_unauthorized(table->getAuthorizer().getRealm());
        return;
    }

    if (!ids.empty()) {
        
        // Fetch single record

        uint64_t id = 0;
        try {
            id = std::stoul(ids);
        } catch(...) {
            LOG4CXX_WARN(logger, "Illegal id: " << ids);
        }
        LOG4CXX_DEBUG(logger, "Requesting record " << id << " from " << tablename << ".");

        auto record = table->getRecord(id);
        if (record != nullptr) {
            nlohmann::json json = record->json();
            LOG4CXX_DEBUG(logger, "Record found: " << json);
            std::string s(json.dump());
            send_response(HttpResponseCode(HttpResponseCode::OK), "text/json", s.c_str(), s.size());
        } else {
            LOG4CXX_DEBUG(logger, "File not found.");
            send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
        }

    } else {

        // Fetch multiple records

        // Start composing the where clause
        Where where;
        for (auto tablecolumn : table->getColumns()) {
            auto value = request.getQueryValueAsString(tablecolumn.columnName);
            if (value.has_value()) {
                where.addTest(tablecolumn.columnName, value.value());
            }
        }

        nlohmann::json main;
        nlohmann::json json;
        auto records = table->getRecords(where);
        int page = request.getQueryValueAsInt("page").value_or(0);
        int defaultsize = Configuration::getInstance()->getInstance()->getInteger("default_pagesize").value_or(DEFAULT_PAGESIZE);
        int size = request.getQueryValueAsInt("size").value_or(defaultsize);
        int i = 0;
        for (auto it = records.begin() + (page * size); (it < records.end()) && (i < size) ; it++, i++) {
            json.push_back((*it)->json());
        }
        int total_hits = records.size();
        int number_of_pages = (total_hits + size - 1) / size;
        main["content"] = json;
        main["totalHits"] = total_hits;
        main["hitsOnPage"] = i;
        main["pageNumber"] = page;
        main["pageSize"] = size;
        main["numberOfPages"] = number_of_pages;
        main["isFirst"] = (page == 0);
        main["isLast"] = ((page + 1) == number_of_pages);
        std::string s(main.dump());
        send_response(HttpResponseCode(HttpResponseCode::OK), "text/json", s.c_str(), s.size());
    }
}

void HttpRunner::handle_db_post_request()
{
    std::string tablename = request.getPathPart(1);
    auto database = Database::getInstance();
    auto table = database->getTable(tablename);
    if (table == nullptr) {
        LOG4CXX_DEBUG(logger, "Table does not exist.");
        send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
        return;
    }

    // Check authorization
    if (!table->getAuthorizer().isAuthorized(authorization, true)) {
        send_unauthorized(table->getAuthorizer().getRealm());
        return;
    }

    // Create record and initialize it with the json record from the body.
    std::shared_ptr<Record> record;
    nlohmann::json json;
    try {
        json = nlohmann::json::parse(body);
        record = std::shared_ptr<Record>(new Record(*table));

    } catch (...) {
        LOG4CXX_DEBUG(logger, "Invalid json in request");
        send_response(HttpResponseCode(HttpResponseCode::BAD_REQUEST), "text/html", RESPONSE_400, sizeof(RESPONSE_400));
        return;
    }

    record->init(json);
    record = table->addRecord(record);
    LOG4CXX_DEBUG(logger, "New record = " << record->csv());
    send_response(HttpResponseCode::OK, "text/json", record->json().dump().data(), record->json().dump().size());
}

void HttpRunner::handle_db_put_request()
{
    std::string tablename = request.getPathPart(1);
    std::string ids = request.getPathPart(2);
    auto database = Database::getInstance();
    auto table = database->getTable(tablename);
    if (table == nullptr) {
        LOG4CXX_DEBUG(logger, "Table does not exist.");
        send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
        return;
    }

    // Check authorization
    if (!table->getAuthorizer().isAuthorized(authorization, true)) {
        send_unauthorized(table->getAuthorizer().getRealm());
        return;
    }

    if (!ids.empty()) {
        
        // Updating single record

        uint64_t id = 0;
        try {
            id = std::stoul(ids);
        } catch(...) {
            LOG4CXX_WARN(logger, "Illegal id: " << ids);
            send_response(HttpResponseCode(HttpResponseCode::BAD_REQUEST), "text/html", RESPONSE_400, sizeof(RESPONSE_400));
            return;
        }
        LOG4CXX_DEBUG(logger, "Updating record " << id << " of " << tablename << ".");

        // Fetch record and update it.
        std::shared_ptr<Record> record = table->getRecord(id);
        nlohmann::json json;
        try {
            json = nlohmann::json::parse(body);
        } catch (...) {
            LOG4CXX_DEBUG(logger, "Invalid json in request");
            send_response(HttpResponseCode(HttpResponseCode::BAD_REQUEST), "text/html", RESPONSE_400, sizeof(RESPONSE_400));
            return;
        }

        record->update(json);
        LOG4CXX_DEBUG(logger, "Updated record = " << record->csv());
        send_response(HttpResponseCode::OK, "text/json", record->json().dump().data(), record->json().dump().size());
    } else {
        // No ID.
        send_response(HttpResponseCode(HttpResponseCode::BAD_REQUEST), "text/html", RESPONSE_400, sizeof(RESPONSE_400));
    }
}

void HttpRunner::handle_db_delete_request()
{
    std::string tablename = request.getPathPart(1);
    std::string ids = request.getPathPart(2);
    auto database = Database::getInstance();
    auto table = database->getTable(tablename);
    if (table == nullptr) {
        LOG4CXX_DEBUG(logger, "Table does not exist.");
        send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
        return;
    }

    // Check authorization
    if (!table->getAuthorizer().isAuthorized(authorization, true)) {
        send_unauthorized(table->getAuthorizer().getRealm());
        return;
    }

    if (!ids.empty()) {
        
        // Fetch single record

        uint64_t id = 0;
        try {
            id = std::stoul(ids);
        } catch(...) {
            LOG4CXX_WARN(logger, "Illegal id: " << ids);
        }
        LOG4CXX_DEBUG(logger, "Deleting record " << id << " from " << tablename << ".");

        if (table->deleteRecord(id)) {
            send_response(HttpResponseCode(HttpResponseCode::OK), "text/html", RESPONSE_200, sizeof(RESPONSE_200));
        } else {
            LOG4CXX_DEBUG(logger, "Record not found.");
            send_response(HttpResponseCode(HttpResponseCode::NOT_FOUND), "text/html", RESPONSE_404, sizeof(RESPONSE_404));
        }
    }
}

bool HttpRunner::check_redirection(std::vector<std::string>& lines)
{
    // Meta file is loaded. React based on the contents.
    // Return true, if the original request should be handled.

    for (std::string line : lines) {
        if ((line.size() > 4) && line.substr(0,4) == "302,") {
            // Temporary redirect
            std::string location = line.substr(4);
            LOG4CXX_INFO(logger, "Temporary redirect to: " << location);
            send_redirect_response(false, location);
            return false;
        }
        if ((line.size() > 4) && line.substr(0,4) == "301,") {
            // Permanent redirect
            std::string location = line.substr(4);
            LOG4CXX_INFO(logger, "Permanent redirect to: " << location);
            send_redirect_response(true, location);
            return false;
        }
    }
    return true;
}

bool HttpRunner::parse_headers()
{
    std::string_view sv(payload, payload_size);
    size_t start = 0;
    bool firstline = true;
    while (start < payload_size) {
        size_t delimeter = sv.find('\n', start);

        if (delimeter == std::string::npos) {
            return false;
        }

        // Get start of next header
        size_t next = delimeter + 1;
        if (sv[--delimeter] != '\r') delimeter++;

        // Check if end of headers found (\r\n\r\n).
        if (delimeter == start) {
            body = payload + next;
            return true;
        }

        parse_header_line(std::string(sv.substr(start, delimeter - start)), firstline);
        start = next;
        firstline = false;
    }

    // Return false because no end of headers found.
    return false;
}

bool HttpRunner::parse_header_line(const std::string line, bool firstline)
{
    if (firstline) {
        auto parts = split(line, ' ');
        if (parts.size() == 3) {
            std::string method = parts[0];
            std::string url = parts[1];
            protocol = parts[2];
            request.init(method, url);
        } else {
            return false;
        }
    } else {
        auto delimeter = line.find_first_of(':');
        if (delimeter != std::string::npos) {
            std::string key = line.substr(0, delimeter);
            std::string value = line.substr(delimeter + 1);
            ltrim(value);

            if (strings_equal_ignore_case(key, "Host")) {
                host = value;
            } else if (strings_equal_ignore_case(key, "User-Agent")) {
                user_agent = value;
            } else if (strings_equal_ignore_case(key, "Accept")) {
                accept = value;
            } else if (strings_equal_ignore_case(key, "Accept-Language")) {
                accept_language = value;
            } else if (strings_equal_ignore_case(key, "Connection")) {
                connection = value;
            } else if (strings_equal_ignore_case(key, "Authorization")) {
                authorization = value;
            }
        } else {
            return false;
        }
    }
    return true;
}

void HttpRunner::send_test_response()
{
    std::stringstream ss;

    ss << "Chinook " << VERSION << "\r\n";
    ss << "Url: " << request.getPath() << "\r\n";
    ss << "Protocol: " << protocol << "\r\n";
    ss << "Host: " << host << "\r\n";
    ss << "Accept: " << accept << "\r\n";
    ss << "Accept-Language: " << accept_language << "\r\n";
    ss << "Conmection: " << connection << "\r\n";
    ss << "Authorization: " << authorization << "\r\n";
    send_response(HttpResponseCode(HttpResponseCode::OK), "text/plain", ss.str().data(), ss.str().size());
}

void HttpRunner::send_unauthorized(const std::string& realm)
{
    std::string content(RESPONSE_401);
    std::stringstream ss;
    ss << "HTTP/1.1 401 Unauthorized\r\n";
    ss << "Server: Chinook-" << VERSION << "\r\n";
    ss << "Content-Type: text/html; charset=UTF-8\r\n";
    ss << "Content-Length: " << content.size() << "\r\n";
    ss << "WWW-Authenticate: Basic realm=\"" << realm << "\"\r\n";
    ss << "\r\n";
    send_response(ss.str(), content.data(), content.size());
}

void HttpRunner::send_response(HttpResponseCode code, std::string content_type, const char* data, const size_t size)
{
    std::stringstream ss;
    ss << "HTTP/1.1 " << code.toString() << "\r\n";
    ss << "Server: Chinook-" << VERSION << "\r\n";
    if (!content_type.empty()) {
        ss << "Content-Type: " << content_type << "; charset=UTF-8\r\n";
    }
    if (size > 0) {
        ss << "Content-Length: " << size << "\r\n";
    }
    ss << "\r\n";
    send_response(ss.str(), data, size);
}

void HttpRunner::send_response(std::string header, const char* data, const size_t size)
{
    size_t header_size = header.length();
    size_t send_size = header_size + size;
    if (send_size > MAX_PAYLOAD_SIZE) {
        LOG4CXX_ERROR(logger, "Cannot send data because send buffer is too small.");
    } else {
        header.copy(send_buffer, header_size);
        if (size) {
            memcpy(send_buffer + header_size, data, size);
        }
        if (ssl) {
            SSL_write(ssl, send_buffer, send_size);
        } else {
            send(socket_descriptor, send_buffer, send_size, 0);
        }
    }
}

void HttpRunner::send_redirect_response(bool permanent, std::string location)
{
    std::string content(permanent ? RESPONSE_301 : RESPONSE_302);
    content = string_replace(content, "$location", location);
    std::stringstream ss;
    if (permanent) {
        ss << "HTTP/1.1 301 Permanently Moved\r\n";
    } else {
        ss << "HTTP/1.1 302 Temporary Moved\r\n";
    }
    ss << "Server: Chinook-" << VERSION << "\r\n";
    ss << "Location: " << location << "\r\n";
    ss << "Content-Type: text/html; charset=UTF-8\r\n";
    ss << "Content-Length: " << content.size() << "\r\n";
    ss << "\r\n";
    send_response(ss.str(), content.c_str(), content.size());
}
