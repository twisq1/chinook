#include <filesystem>
#include <fstream>
#include "utils.h"
#include "config.h"
#include "configuration.h"
#include "table.h"

Table::Table(std::string name) :
    tableName(name)
{
    logger = log4cxx::Logger::getLogger("table");
    maxid = 0;
    dirty = false;
    LOG4CXX_DEBUG(logger, "Table object '" << tableName << "' instantiated");
}

Table::~Table()
{
    LOG4CXX_DEBUG(logger, "Table object '" << tableName << "' destroyed.");
}

bool Table::load()
{
    std::string basepath = Configuration::getInstance()->getString("database_directory").value_or(DEFAULT_DATABASE_DIRECTORY);
    std::filesystem::path metafilename(basepath);
    metafilename /= tableName + ".meta";
    std::ifstream meta(metafilename);
    if (meta.is_open()) {
        std::vector<std::string> lines;
        std::string line;
        while (getline(meta, line)) {
            lines.push_back(line);
        }
        meta.close();
        if (lines.size()) {
            authorizer.init(lines);
        }
        LOG4CXX_INFO(logger, "Table authorizer initialized for table " << tableName);
    }

    std::filesystem::path p(basepath);
    p /= tableName + ".csv";

    std::ifstream file(p);
    std::string line;
    int linenr = 1;
    if (file.is_open()) {
        while (getline(file, line)) {
            if ((line.size() > 0) && (line[0] == '#')) {
                for (std::string field : smart_split(line.substr(1))) {
                    ColumnDefinition coldef;
                    removeQuotes(field);
                    coldef.columnName = field;
                    coldef.columnType = Field::UNKNOWN;
                    columns.push_back(coldef);
                }
            } else {
                auto new_record = std::shared_ptr<Record>(new Record(*this));
                bool record_ok = true;
                auto  it = columns.begin();
                for (std::string value : smart_split(line)) {
                    if (it == columns.end()) {
                        LOG4CXX_WARN(logger, "Record size is bigger than header size.");
                        record_ok = false;
                        break;
                    }
                    auto field = std::shared_ptr<Field>(new Field(value));
                    if (it->columnType == Field::UNKNOWN) {
                        it->columnType = field->getType();
                    } else {
                        if ((field->getType() != Field::UNKNOWN) && (field->getType() != it->columnType)) {
                            LOG4CXX_WARN(logger, "Field type does not match in table " << tableName << ", line " << linenr);
                            record_ok = false;
                            break;
                        }
                    }
                    new_record->addField(field);
                    it++;
                }
                if (record_ok) {
                    if (new_record->hasId()) {
                        auto id = new_record->getId();
                        if (id > maxid) maxid = id;
                        records.insert(std::pair<uint64_t, std::shared_ptr<Record>>(id, new_record));
                    } else {
                        LOG4CXX_WARN(logger, "Record on line " << linenr << " has no valid id.");
                    }
                }
            }
            linenr++;
        }
        return true;
    }
    return false;
}

bool Table::save()
{
    if (dirty) {
        // TODO: Add error handling and make old database file.

        // Open file
        std::string basepath = Configuration::getInstance()->getString("database_directory").value_or(DEFAULT_DATABASE_DIRECTORY);
        std::filesystem::path p(basepath);
        p /= tableName + ".csv";
        std::ofstream file(p);

        // Write header
        std::stringstream ss;
        bool first = true;
        for (auto column : columns) {
            if (first) ss << "#";
            else ss << ",";
            ss << column.columnName;
            first = false;
        }
        ss << std::endl;
        file << ss.str();

        // Write records
        for (auto record : records) {
            file << record.second->csv() << std::endl;
        }
        file.close();

        LOG4CXX_DEBUG(logger, "Table " << tableName << " saved.");
        dirty = false;
    }
    return true;
}

std::shared_ptr<Record> Table::getRecord(uint64_t id)
{
    std::lock_guard guard(mut);
    return records[id];
}

std::vector<std::shared_ptr<Record>> Table::getRecords(Where& where)
{
    std::lock_guard guard(mut);
    std::vector<std::shared_ptr<Record>> allrecords;
    for (auto item : records) {
        if (item.second->check(where)) {
            allrecords.push_back(item.second);
        }
    }
    return allrecords;
}

std::shared_ptr<Record> Table::addRecord(std::shared_ptr<Record> record)
{
    std::lock_guard guard(mut);
    record->setId(++maxid);
    records.insert(std::pair<uint64_t, std::shared_ptr<Record>>(record->getId(), record));
    dirty = true;
    return record;
}

std::vector<std::string> Table::smart_split(const std::string line) const
{
    std::stringstream stream(line);
    std::stringstream item;
    std::vector<std::string> elements;
    bool escape_mode = false;
    bool string_mode = false;

    for (char c : line) {
        if (escape_mode) {
            item << c;
            escape_mode = false;
        } else {
            if (c == '\\') {
                escape_mode = true;
            } else if ((c == ',') && !string_mode) {
                elements.push_back(item.str());
                item.str(std::string());            // Make item empty.
                string_mode = false;
            } else {
                item << c;
                if (c == '"') {
                    string_mode = !string_mode;
                }
            }
        }
    }
    elements.push_back(item.str());
    return elements;
}

bool Table::deleteRecord(uint64_t id)
{
    std::lock_guard guard(mut);
    auto it = records.find(id);
    if (it == records.end()) {
        return false;
    }
    records.erase(it);
    dirty = true;
    return true;
}