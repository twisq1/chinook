#include <filesystem>
#include <fstream>
#include "filegetter.h"

FileGetter::FileGetter(const std::string document_root)
: root(document_root)
{
    logger = log4cxx::Logger::getLogger("filegetter");
    buffer = nullptr;
    size = 0;
}

FileGetter::~FileGetter()
{
    if (buffer) delete[] buffer;
    //LOG4CXX_DEBUG(logger, "File Getter destroyed.");
}

std::string FileGetter::getMime() const
{
    return mime;
}

char* FileGetter::getData() const
{
    return buffer;
}

size_t FileGetter::getSize() const
{
    return size;
}

bool FileGetter::load(std::string host, std::string request)
{
    // Delete memory from previous time.
    if (buffer) delete[] buffer;

    if (request.size() && request[0] == '/') {
        url = request.substr(1);
    } else {
        url = request;
    }

    // Remove port number from host.
    size_t colon = host.find(':');
    if (colon != std::string::npos) {
        host = host.substr(0, colon);
    }

    std::filesystem::path path(root);
    path /= host;
    path /= url;
    LOG4CXX_DEBUG(logger, "Loading file: " << path);
    if (std::filesystem::exists(path)) {

        if (std::filesystem::is_directory(path)) {
            LOG4CXX_DEBUG(logger, "Appending index.html to path");
            path /= "index.html";
        }

        std::string extension = path.extension();
        if (extension == ".html") {
            mime = "text/html";
        } else if ((extension == ".log") || (extension == ".txt") || (extension == ".meta")) {
            mime = "text/plain";
        } else if (extension == ".png") {
            mime = "image/png";
        } else if (extension == ".jpg") {
            mime = "image/jpeg";
        } else if (extension == ".css") {
            mime = "text/css";
        } else if (extension == ".js") {
            mime = "text/javascript";
        } else if (extension == ".ico") {
            mime = "image/vnd.microsoft.icon";
        } else {
            LOG4CXX_WARN(logger, "Don't know mime type for extension " << extension);
            mime = "application/octet-stream";
        }

        size = std::filesystem::file_size(path);

        // Allocate memory for file.
        buffer = new char[size];

        std::ifstream file;
        file.open(path, std::ios::binary);
        file.read(buffer, size);
        file.close();

        return true;
    }
    return false;
}
