// Many thanks to:
// https://beej.us/guide/bgnet/html/#intro
// https://wiki.openssl.org/index.php/Simple_TLS_Server

#include <cstring>
#include <string>   
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <chrono>
#include "dispatcher.h"
#include "configuration.h"
#include "httprunner.h"
#include "database.h"
#include "config.h"
#include "chinook.h"

using namespace std::chrono_literals;

void Chinook::start()
{
    // Start runner in background thread.
    my_thread = std::thread(&Chinook::run, this);
}

void Chinook::stop()
{
    // Stop runner.
    on = false;
}

Chinook::Chinook(int port, bool tls, const std::string document_root, int workers) :
    port(port),
    tls(tls),
    document_root(document_root),
    workers(workers)
{
    logger = log4cxx::Logger::getLogger(tls ? "chinook-https" : "chinook-http");
    sockFD = 0;
    on = true;
}

Chinook::~Chinook()
{
    stop();
    close(sockFD);
    my_thread.join();
    LOG4CXX_DEBUG(logger, "Chinook runner stopped.");
}

void Chinook::init_openssl()
{ 
    SSL_load_error_strings();	
    OpenSSL_add_ssl_algorithms();
}

void Chinook::cleanup_openssl()
{
    EVP_cleanup();
}

SSL_CTX* Chinook::get_context(const std::string servername)
{
    auto it = ssl_contexts.find(servername);
    if (it == ssl_contexts.end()) {

        LOG4CXX_DEBUG(logger, "Create SSL context for servername: " << servername);
        
        const SSL_METHOD *method;
        SSL_CTX *ctx;

        method = SSLv23_server_method();

        ctx = SSL_CTX_new(method);
        if (!ctx) {
            LOG4CXX_ERROR(logger, "Unable to create SSL context");
            exit(EXIT_FAILURE);
        }

        SSL_CTX_set_ecdh_auto(ctx, 1);

        /* Set the key and cert */
        std::string certificate_filename = servername + CERTIFICATE_FILENAME_POSTFIX;
        if (SSL_CTX_use_certificate_file(ctx, certificate_filename.c_str(), SSL_FILETYPE_PEM) <= 0) {
            LOG4CXX_ERROR(logger, "Error with certificate: " << getOpenSSLError());
            //exit(EXIT_FAILURE);
        }

        std::string private_key_filename = servername + PRIVATE_KEY_FILENAME_POSTFIX;
        if (SSL_CTX_use_PrivateKey_file(ctx, private_key_filename.c_str(), SSL_FILETYPE_PEM) <= 0 ) {
            LOG4CXX_ERROR(logger, "Error with private key: " << getOpenSSLError());
            //exit(EXIT_FAILURE);
        }
        ssl_contexts[servername] = ctx;
    }

    return ssl_contexts[servername];
}

// Get errors from OpenSSL lib and put them in s atring.
std::string Chinook::getOpenSSLError()
{
    BIO *bio = BIO_new(BIO_s_mem());
    ERR_print_errors(bio);
    char *buf;
    size_t len = BIO_get_mem_data(bio, &buf);
    std::string ret(buf, len);
    BIO_free(bio);
    return ret;
}

// The SNI call back first calls this helper function with a pointer to the Chinook class as parameter.
int callback_helper(SSL *ssl, int *ad, void *arg)
{
    Chinook* chinook = static_cast<Chinook*>(arg);
    return chinook->servername_callback(ssl);
}

int Chinook::servername_callback(SSL *ssl)
{
    if (ssl == NULL)
        return SSL_TLSEXT_ERR_NOACK;

    const char* servername = SSL_get_servername(ssl, TLSEXT_NAMETYPE_host_name);
    if (!servername || servername[0] == '\0')
        return SSL_TLSEXT_ERR_NOACK;

    LOG4CXX_DEBUG(logger, "SNI, servername = " << servername);

    /* Does the default cert already handle this domain? */
    if (DEFAULT_SERVER == servername)
    {
        //LOG4CXX_DEBUG(logger, "Already default servername.");
        return SSL_TLSEXT_ERR_OK;
    }

    /* Need a new certificate for this domain */
    SSL_CTX* ctx = get_context(servername);
    if (ctx == NULL)
    {
        LOG4CXX_ERROR(logger, "No certificate for this servername: " << servername);
        return SSL_TLSEXT_ERR_NOACK;   
    }

    /* Useless return value */
    SSL_CTX* v = SSL_set_SSL_CTX(ssl, ctx);
    if (v != ctx)
    {
        LOG4CXX_ERROR(logger, "Useless return value for servername: " << servername);
        return SSL_TLSEXT_ERR_NOACK;
    }

    return SSL_TLSEXT_ERR_OK;
}

void Chinook::run()
{
    int backlog = Configuration::getInstance()->getInteger("backlog").value_or(DEFAULT_BACKLOG);
    std::string portNum = std::to_string(port);

    if (tls) {
        init_openssl();

        // Create default SSL context and set callback function for SNI.
        auto ctx = get_context(DEFAULT_SERVER);
        SSL_CTX_set_tlsext_servername_callback(ctx, &callback_helper);
        SSL_CTX_set_tlsext_servername_arg(ctx, this);
    }

    addrinfo hints, *res, *p;                   // we need 2 pointers, res to hold and p to iterate over
    memset(&hints, 0, sizeof(hints));

    // for more explanation, man socket
    hints.ai_family   = AF_UNSPEC;              // Don't specify which IP version to use yet.
    hints.ai_socktype = SOCK_STREAM;            // SOCK_STREAM refers to TCP.
    hints.ai_flags    = AI_PASSIVE;

    // man getaddrinfo
    int gAddRes = getaddrinfo(NULL, portNum.c_str(), &hints, &res);
    if (gAddRes != 0) {
        LOG4CXX_ERROR(logger, gai_strerror(gAddRes));
        return;
    }

    // Now since getaddrinfo() has given us a list of addresses
    // we're going to iterate over them and try to open a socket and bind.
    for (p = res; p != NULL; p = p->ai_next) {

        // let's create a new socket, socketFD is returned as descriptor
        // man socket for more information
        // these calls usually return -1 as result of some error
        sockFD = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sockFD == -1) {
            LOG4CXX_DEBUG(logger, "Error while trying to open socket. errno = " << errno);

        } else {
            // Let's bind address to our socket we've just created.
            // bind returns 0 on success. man bind for more information.
            if (bind(sockFD, p->ai_addr, p->ai_addrlen) == -1) {
                // Error while binding. Try next option.
                int error = errno;
                if (error == 98) {
                    LOG4CXX_DEBUG(logger, "Error while binding socket. Address already in use.");
                } else {
                    LOG4CXX_DEBUG(logger, "Error while binding socket. errno = " << error);
                }
                close(sockFD);
            } else {
                // Socket is open and successfully bind. Stop iteration.
                break;
            }
        }
    }
    freeaddrinfo(res);

    if (p == NULL) {
        LOG4CXX_ERROR(logger, "Unable to open socket and bind to port address.");
        return;
    }

    // finally start listening for connections on our socket.
    int listenR = listen(sockFD, backlog);
    if (listenR == -1) {
        LOG4CXX_ERROR(logger, "Error while Listening on socket.");

        // if some error occurs, make sure to close socket and free resources
        close(sockFD);
        return;
    }

    // Set socket flags to NON-BLOCKING.
    int flags = fcntl(sockFD, F_GETFL);
    fcntl(sockFD, F_SETFL, flags | O_NONBLOCK);

    // Get workers to do the work.
    Dispatcher dispatcher(workers);

    // structure large enough to hold client's address
    sockaddr_storage client_addr;
    socklen_t client_addr_size = sizeof(client_addr);

    // a fresh infinite loop to communicate with incoming connections
    // this will take client connections one at a time
    // in further examples, we're going to use fork() call for each client connection
    LOG4CXX_INFO(logger, "Waiting for incoming connection on port " << port);
    LOG4CXX_INFO(logger, "Connection type: " << (tls ? "SSL/TLS (HTTPS)" : "No SSL/TLS (HTTP)"));
    while (on) {

        // accept call will give us a new socket descriptor
        int newFD = accept(sockFD, (sockaddr *) &client_addr, &client_addr_size);
        if (newFD == -1) {
            int error = errno;
            if (errno == EWOULDBLOCK) {
                // No data available for connection. Wait a while and try again.
                std::this_thread::sleep_for(1ms);
            } else {
                LOG4CXX_ERROR(logger, "Error while Accepting on socket: " << error);
            }
        } else {

            // Set timeout of connection
            struct timeval tv;
            tv.tv_sec = Configuration::getInstance()->getInteger("connection_timeout").value_or(DEFAULT_CONNECTION_TIMEOUT);
            tv.tv_usec = 0;
            setsockopt(newFD, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

            // Open SSL connection.
            SSL* ssl = nullptr;
            bool ssl_ok = false;
            if (tls) {
                ssl = SSL_new(get_context(DEFAULT_SERVER));
                SSL_set_fd(ssl, newFD);
                if (SSL_accept(ssl) <= 0) {
                    LOG4CXX_ERROR(logger, "SSL error: " << getOpenSSLError());
                } else {
                    ssl_ok = true;
                }
            }
            if (!tls || ssl_ok) {
                // We got a new connection. Handle this turn in a separate thread.
                if (client_addr.ss_family == AF_INET) {

                    auto runner = new HttpRunner(document_root, newFD, client_addr.ss_family, &reinterpret_cast<struct sockaddr_in*>(&client_addr)->sin_addr, ssl);
                    dispatcher.give_order(runner);

                } else if (client_addr.ss_family == AF_INET6) {

                    auto runner = new HttpRunner(document_root, newFD, client_addr.ss_family, &reinterpret_cast<struct sockaddr_in6*>(&client_addr)->sin6_addr, ssl);                
                    dispatcher.give_order(runner);

                } else {
                    LOG4CXX_ERROR(logger, "Connection from non IP client.");
                }
            }
        }
    }
    close(sockFD);
    if (tls) {
        for (auto it : ssl_contexts) {
            SSL_CTX_free(it.second);
        }
        cleanup_openssl();
    }
    
    return;
}
