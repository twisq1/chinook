#pragma once

#include <mutex>
#include <queue>
#include <thread>
#include <optional>

template<typename T>
class ThreadsafeQueue
{
public:
    ThreadsafeQueue() :
        mut(),
        data_queue()
    {};

    void push(T new_value)
    {
        std::lock_guard guard(mut);
        data_queue.push_front(new_value);
    };

    std::optional<T> pop()
    {
        std::lock_guard guard(mut);
        if (data_queue.size()) {
            T retval = data_queue.back();
            data_queue.pop_back();
            return std::make_optional(retval);
        }
        return std::nullopt;
    }

    std::size_t size() const
    {
        std::lock_guard guard(mut);
        return data_queue.size();
    }

private:
    std::mutex mut;
    std::deque<T> data_queue;
};
