#include "dispatcher.h"

Dispatcher::Dispatcher(const int number_of_workers)
{
    logger = log4cxx::Logger::getLogger("chinook.dispatcher");
    LOG4CXX_DEBUG(logger, "Initializing dispatcher with " << number_of_workers << " workers.");

    for (int i = 0; i < number_of_workers; i++) {
        workers.push_back(new Worker());
    }
    worker_on_duty = workers.begin();
}

Dispatcher::~Dispatcher()
{
    LOG4CXX_DEBUG(logger, "Destroying dispatcher");
    for (auto it : workers) {
        delete it;
    }
}

void Dispatcher::give_order(Runnable* runnable) {
    (*worker_on_duty)->give_order(runnable);
    if ((++worker_on_duty) == workers.end()) worker_on_duty = workers.begin();
}
