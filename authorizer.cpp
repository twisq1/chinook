#include "utils.h"
#include "database.h"
#include "where.h"
#include "authorizer.h"

Authorizer::Authorizer()
{
    logger = log4cxx::Logger::getLogger("authorizer");
    noInfo = false; // Together with an empty allowedReadGroups, this will fail all requests.
}

void Authorizer::init()
{
    noInfo = true;
    realm = "Chinook";
    allowedReadGroups.clear();
    allowedWriteGroups.clear();
}

void Authorizer::init(const std::vector<std::string>& metafile)
{
    for (auto line : metafile) {
        if (line.substr(0,4) == "401,") {
            auto parts = split(line, ',');
            if (parts.size() >= 3) {
                noInfo = false;
                realm = parts[1];
                allowedReadGroups = split(parts[2], ' ');
                if (parts.size() >= 4) {
                    allowedWriteGroups = split(parts[3], ' ');
                } else {
                    allowedWriteGroups.clear();
                }
                return;
            } else {
                LOG4CXX_ERROR(logger, "Illegal authorization line in meta file.");
                return;
            }
        }
    }
    init();
}

bool Authorizer::isAuthorized(const std::string& authorization_header, bool write)
{
    if (noInfo) return true;
    if (authorization_header.empty()) {
        LOG4CXX_INFO(logger, "Request has no authorization header.");
        return false;
    }

    auto authorization_parts = split(authorization_header, ' ');
    if ((authorization_parts.size() == 2) && (authorization_parts[0] == "Basic")) {

        // Basic Auth 
        auto decoded = jwt::base::decode<jwt::alphabet::base64>(authorization_parts[1]);
        auto namepw = split(decoded, ':');
        if (namepw.size() == 2) {
            auto name = namepw[0];
            auto password = namepw[1];

            auto table = Database::getInstance()->getTable(USERS_TABLE);
            if (table != nullptr) {
                Where where;
                where.addTest(std::string("name"), name);
                auto users = table->getRecords(where);
                if (users.size() > 0) {
                    auto user = users.begin();
                    auto field = (*user)->getValue(std::string("password"));
                    if (field != nullptr) {
                        if (field->getString() == password) {
                            // Password okay, check group
                            auto groups = (*user)->getValue(std::string("groups"));
                            if (groups != nullptr) {
                                for (auto allowedGroup : (write ? allowedWriteGroups : allowedReadGroups)) {
                                    if ((allowedGroup == "*") || (groups->getString().find(allowedGroup) != std::string::npos)) {
                                        LOG4CXX_INFO(logger, "User " << name << " is authorized.");
                                        return true;
                                    }
                                }
                                LOG4CXX_INFO(logger, "User " << name << " is not authorized.");
                                return false;
                            } else {
                                LOG4CXX_ERROR(logger, "User table has no group column.");
                            }
                        } else {
                            // Password not okay.
                            return false;
                        }
                    } else {
                        LOG4CXX_ERROR(logger, "User table has no password column.");
                    }
                } else {
                    LOG4CXX_INFO(logger, "User " << name << " does not exist.");
                }
            } else {
                LOG4CXX_WARN(logger, "Users table not found.");
            }

        } else {
            LOG4CXX_INFO(logger, "No valid password found.");
        }
    } else {
        LOG4CXX_DEBUG(logger, "Unknown authorization method.");
    }
    return false;
}

std::string Authorizer::getRealm() const
{
    return realm;
}
