#pragma once

#include <string>
#include <log4cxx/logger.h>

class FileGetter
{
public:
    FileGetter(std::string document_root);
    virtual ~FileGetter();

    bool load(std::string host, std::string url);
    std::string getMime() const;
    char* getData() const;
    size_t getSize() const;

private:
    log4cxx::LoggerPtr logger;
    std::string root;
    std::string url;
    std::string mime;
    size_t size;
    char* buffer;
};