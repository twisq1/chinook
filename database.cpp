#include <filesystem>
#include "config.h"
#include "configuration.h"
#include "database.h"

std::shared_ptr<Database> Database::instance = nullptr;

std::shared_ptr<Database> Database::getInstance()
{
    if (!instance) {
        instance = std::shared_ptr<Database>(new Database);
    }
    return instance;
}

Database::Database()
{
    logger = log4cxx::Logger::getLogger("database");
    LOG4CXX_DEBUG(logger, "Database object created.");
}

Database::~Database()
{
    LOG4CXX_DEBUG(logger, "Database object destroyed.");
}

void Database::load()
{
    // Test if database directory does exist.
    std::string database_directory = Configuration::getInstance()->getString("database_directory").value_or(DEFAULT_DATABASE_DIRECTORY);
    std::filesystem::path db_path(database_directory);
	if (!std::filesystem::is_directory(db_path)) {
		LOG4CXX_WARN(logger, "Database directory does not exist: " << database_directory);
	} else {

        // Look for csv files in directory.
        for (auto& entry : std::filesystem::directory_iterator(db_path)) {
		    
            // Do not process hidden files.
		    if (entry.path().filename().string().front() != '.') {
                
                // Only process csv files.
                if (entry.path().extension() == ".csv") {
                    std::string tablename = entry.path().stem();
                    auto newtable = std::shared_ptr<Table>(new Table(tablename));
                    if (newtable->load()) {
                        tables.insert(std::pair<std::string, std::shared_ptr<Table>>(tablename, newtable));
                    }
                }
            }
        }
    }
}

std::shared_ptr<Table> Database::getTable(std::string tablename)
{
    return tables[tablename];
}

std::vector<std::shared_ptr<Table>> Database::getTables()
{
    std::vector<std::shared_ptr<Table>> retval;
    for (auto it : tables) {
        retval.push_back(it.second);
    }
    return retval;
}
