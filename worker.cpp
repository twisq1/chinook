#include <optional>
#include <chrono>
#include "worker.h"

using namespace std::chrono_literals;

int Worker::thread_nr = 1;

Worker::Worker() :
    queue(),
    working(true)
{
    std::string logger_name = "chinook.worker." + std::to_string(thread_nr++);
    logger = log4cxx::Logger::getLogger(logger_name);
    my_thread = std::thread(&Worker::job_handler, this);
}
    
Worker::~Worker()
{
    working = false;
    my_thread.join();
    //LOG4CXX_DEBUG(logger, "Worker destructed.");
}

void Worker::job_handler()
{
    //LOG4CXX_DEBUG(logger, "Waiting for orders.");
    while (working) {
        std::optional<Runnable*> runnable = queue.pop();
        if (runnable.has_value()) {
            (*runnable)->operator()();
            delete *runnable;
        }
        std::this_thread::sleep_for(1ms);
    }
}

void Worker::give_order(Runnable* runnable)
{
    queue.push(runnable);
}