#pragma once

#include <memory>
#include <vector>
#include <string>
#include "log4cxx/logger.h"
#include "json.h"
#include "where.h"
#include "field.h"

class Table;

class Record
{
public:
    Record(Table& table);
    virtual ~Record();

    void addField(std::shared_ptr<Field> field);
    uint64_t getId() const;
    void setId(uint64_t x);
    std::shared_ptr<Field> getValue(int n) const;
    std::shared_ptr<Field> getValue(const std::string& key) const;
    bool hasId() const;
    std::string str() const;
    nlohmann::json json() const;
    std::string csv() const;
    bool init(nlohmann::json& json);
    void update(nlohmann::json& json);

    // Check if the where statement is true for this record.
    bool check(Where& where);

private:
    Table& table;
    log4cxx::LoggerPtr logger;
    std::vector<std::shared_ptr<Field>> values;
    std::shared_ptr<Field> id;
};