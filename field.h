#pragma once

#include <stdint.h>
#include <string>
#include <variant>
#include <optional>

class Field
{
public:
    enum FieldType {
        UNKNOWN,
        INTEGER,
        FLOAT,
        BOOLEAN,
        STRING
    };

    Field();
    Field(const std::string& s);

    FieldType getType() const;

    int64_t getInteger() const { return std::get<int64_t>(value); };
    double getFloat() const { return std::get<double>(value); };
    bool getBool() const { return std::get<bool>(value); };
    std::string getString() const { return std::get<std::string>(value); };
    
    void setValue(int64_t x);
    void setValue(double x);
    void setValue(bool x);
    void setValue(std::string x);
    void setNull();

    std::string convertToString() const;

    bool operator==(const Field& y) const;

private:
    std::variant<int64_t, double, bool, std::string, std::nullopt_t> value;
};